echo '--- Installing Helm'
sudo snap install helm --classic
helm init --wait --upgrade

echo '--- Adding qlik-stable helm repo'
helm repo add qlik-stable https://qlik.bintray.com/stable
helm repo update

echo '---  Installing QlikSense from Stable repo'
helm install -n qliksense-init qlik-stable/qliksense-init
helm upgrade --install qliksense qlik-stable/qliksense -f /vagrant/files/values.yaml 