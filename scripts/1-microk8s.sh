# MicroK8s: To workaround issue with PODs not solving public DNS
sudo iptables -P FORWARD ACCEPT

echo '--- Installing MicroK8s'
sudo snap install microk8s --classic
sudo snap alias microk8s.kubectl kubectl
sudo microk8s.status --wait-ready
# Enable dns and storage
sudo microk8s.enable dns
sleep 10
sudo microk8s.enable storage
sleep 10

sudo mkdir -p $HOME/.kube/
sudo microk8s.kubectl config view --raw > config
sudo mv config $HOME/.kube/config
sudo chown -R ${USER}:${USER} $HOME/.kube/


