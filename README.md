# QSE on Kubernetes (MicroK8s)
## Description
A local demonstration environment for showing the benefits of Qlik Sense Enterprise on Kubernetes using MicroK8s as cluster. Auth0 is used as the IdP. Persistence is enabled for mayor container (mongo, redis, engine, library)

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-qseok-microk8s | 192.168.58.101   | Qlik Sense Enterprise on Kubernetes |

## Hosts file
It is recommended that you add the following to both your laptops HOSTS file 

```
192.168.58.101 qmi-qseok-microk8s
```

And to any Qlik Sense Enterprise for Windows servers that will distribute content.

The hosts file is found here: c:\windows\system32\drivers\etc

## Linux Users
| Name | Password |
|------|-----|
|vagrant|vagrant|


## Connection
Please use __vagrant ssh__ to connect to the server (or if using your own SSH use ssh vagrant@qmi-qseok-microk8s to connect (password: vagrant))

You can monitor PODs by executing 

```
kubectl get pod -w
```

```
NAME                                                 READY   STATUS    RESTARTS   AGE
qliksense-audit-546dcd7747-fcxdl                     1/1     Running   8          23m
qliksense-chronos-7b8745557b-gd6n8                   2/2     Running   0          23m
qliksense-chronos-worker-685f95b4d-ppvsm             1/1     Running   6          23m
qliksense-collections-6f75cc7c55-sgk7m               1/1     Running   0          23m
qliksense-data-connector-odbc-cmd-5dbf6b895-wm758    1/1     Running   0          23m
qliksense-data-connector-odbc-rld-78d449cbb9-fbgqc   1/1     Running   0          23m
qliksense-data-connector-qwc-cmd-f64777fc8-vlnkn     1/1     Running   7          23m
qliksense-data-connector-qwc-rld-77955bc889-zw97d    1/1     Running   8          23m
qliksense-data-connector-rest-cmd-86c9f7b564-zs96h   1/1     Running   0          23m
qliksense-data-connector-rest-rld-6bf7cdcfd4-twj4z   1/1     Running   0          23m
qliksense-data-prep-5bd4996655-rwtng                 1/1     Running   0          23m
qliksense-data-rest-source-7d5d9cb546-p8bhg          1/1     Running   0          23m
qliksense-dcaas-5644ffcb55-nhzlb                     1/1     Running   0          23m
qliksense-dcaas-redis-master-0                       1/1     Running   0          23m
qliksense-dcaas-redis-metrics-68d9bdf575-4pstr       1/1     Running   0          23m
qliksense-dcaas-web-798b5f54ff-lsssg                 1/1     Running   0          23m
qliksense-edge-auth-5dbb4d5658-5kwrq                 2/2     Running   1          23m
qliksense-encryption-d759bc94c-5z72b                 1/1     Running   0          23m
qliksense-engine-57c47988f-q99mp                     1/1     Running   5          23m
qliksense-feature-flags-785fbfbf84-dfn5b             1/1     Running   0          23m
qliksense-groups-55b58b6659-cssqh                    1/1     Running   6          23m
qliksense-hub-6579844c86-t57zb                       1/1     Running   0          23m
qliksense-identity-providers-78c4bbccd-2fzs9         1/1     Running   0          23m
qliksense-insights-644b549f74-2nw2l                  1/1     Running   5          23m
qliksense-keys-d76885845-g7fsb                       1/1     Running   0          23m
qliksense-licenses-644c67c946-2wkgs                  2/2     Running   0          23m
qliksense-locale-d85fd7c66-kkn5d                     1/1     Running   0          23m
qliksense-management-console-7f795f95d7-bc6v7        1/1     Running   0          23m
qliksense-mongodb-6b4c9bbfdf-zxcbp                   1/1     Running   0          23m
qliksense-nats-0                                     2/2     Running   0          23m
qliksense-nats-streaming-0                           2/2     Running   2          23m
qliksense-nats-streaming-1                           2/2     Running   0          6m
qliksense-nats-streaming-2                           2/2     Running   0          5m27s
qliksense-nginx-ingress-controller-5fc9cdf57-9kl56   1/1     Running   0          23m
qliksense-odag-769bd7777-bfqnz                       2/2     Running   0          23m
qliksense-policy-decisions-7f7dc669c6-jhdwl          1/1     Running   0          23m
qliksense-precedents-565cb45df-qlcrg                 1/1     Running   1          23m
qliksense-qix-data-connection-7f487987fc-7pv9h       1/1     Running   3          23m
qliksense-qix-datafiles-5659ddd966-qf7gv             1/1     Running   0          23m
qliksense-qix-sessions-756bc865d9-r4sz7              1/1     Running   1          23m
qliksense-redis-master-0                             1/1     Running   0          23m
qliksense-redis-slave-578c9894c9-q6mct               1/1     Running   1          23m
qliksense-reload-tasks-8b9bf8fb8-w9rlk               1/1     Running   4          23m
qliksense-reloads-7c64455cbc-r5hl6                   1/1     Running   7          23m
qliksense-reporting-7cf69db7db-m7drl                 4/4     Running   0          23m
qliksense-resource-library-54c779d57-kgs67           1/1     Running   0          23m
qliksense-sense-client-669dc5ccd4-czck5              1/1     Running   0          23m
qliksense-spaces-db7d4c489-tgj2c                     1/1     Running   0          23m
qliksense-temporary-contents-858675f9c6-b2vcg        1/1     Running   0          23m
qliksense-tenants-7c49944cdb-kgcg5                   1/1     Running   0          23m
qliksense-users-5fbb5dd45c-9s9fv                     1/1     Running   0          23m
qmistorage-nfs-client-provisioner-69f9cd8988-fpb2c   1/1     Running   0          23m
```

## Purpose
This scenario provisions the latest stable release of Qlik Sense Enterprise on Kubernetes running in a MicroK8s provisionined K8s cluster. Persistence is enabled.


## What is installed
### Software
1. Ubuntu 18.04
4. MicroK8s
5. Helm (package manager for Kubernetes)
6. Qlik Sense Enterprise on Kubernetes (QSEoK) from "stable" Bintray repository.

## URLs

| Name | URL |
|------|-----|
|QSEoK hub|[https://qmi-qseok-microk8s:32443](https://qmi-qseok-microk8s:32443)|
|QSEoK console|[https://qmi-qseok-microk8s:32443/console](https://qmi-qseok-microk8s:32443/console)|
|QSEoK API license assignments|[https://qmi-qseok-microk8s:32443/api/v1/licenses/assignments](https://qmi-qseok-microk8s:32443/api/v1/licenses/assignments)|
|QSEoK API current logged-in user|[https://qmi-qseok-microk8s:32443/api/v1/users/me](https://qmi-qseok-microk8s:32443/api/v1/users/me)|

## Available Users in Auth0
The following table outlines users that have been created and can be used within the QSEfE hub and QSEfW through SAML.

| User | Password | Groups |
|------|----------|--------|
|harley@qlikcloud.com|Password1!|Everyone, Sales|
|barb@qlikcloud.com|Password1!|Everyone, Support|
|quinn@qlikcloud.com|Password1!|Everyone, Accounting|
|sim@qlikcloud.com|Password1!|Everyone, Accounting|
|phillie@qlikcloud.com|Password1!|Everyone, Marketing, Sales|
|peta@qlikcloud.com|Password1!|Everyone, Engineering|
|marne@qlikcloud.com|Password1!|Everyone, Marketing|
|sibylla@qlikcloud.com|Password1!|Everyone, Accounting|
|evan@qlikcloud.com|Password1!|Everyone, Engineering|
|franklin@qlikcloud.com|Password1!|Everyone, Sales|


Check that all QSEfE Pods are at __Running__ status. SSH into the VM and execute: ```kubectl get pod```




## Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Manuel Romero|1.0|25 July 2019|
